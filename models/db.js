'use strict';

require('dotenv').config();

var mysql = require('mysql');

// local mysql db connection
var connection = mysql.createConnection({
    host: process.env.DB_HOST || "localhost",
    user: process.env.DB_USER || "root",
    password: process.env.DB_PASS || "",
    database: process.env.DB_NAME || "techbase_company"
});

// connect to database
connection.connect(function(err) {
    if (err)  throw err;
});

module.exports = connection;