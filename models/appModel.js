'use strict';

var sql = require('./db');

// Employee object constructor
var Employee = function(employee) {
    this.name = employee.name;
    this.phone = employee.phone;
    this.email = employee.email;
    this.address = employee.address;
    this.age = employee.age;
    this.position = employee.position;
    this.department = employee.department;
    this.created_at = new Date();
};

Employee.getAllEmployee = function getAllEmployee(result) {
    sql.query("SELECT * FROM employees LIMIT 1500", function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log("employees: ", res);
            result(null, res);
        }
    });
};

module.exports = Employee;