'use strict';

module.exports = function(app) {
    var employees = require('../controllers/appController');

    //List Employees Routes
    app.route('/employees')
        .get(employees.list_employees)

};