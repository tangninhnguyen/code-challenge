require('dotenv').config();

const https = require("https"),
    fs = require("fs"),
    express = require('express'),
    helmet = require('helmet'),
    app = express(),
    bodyParser = require('body-parser');

port = process.env.PORT || 3000;
module.exports = app.listen(port);

console.log('API server started on: ' + port);
app.use(helmet()); // Add Helmet as a middleware
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var routes = require('./routes/appRoutes');
routes(app);