// Require the dev-dependencies
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();

chai.use(chaiHttp);
//Test-case parent block
describe('Employees', function() {
    // Test the /GET route
    it('should list All employees on /employees GET', function(done) {
        chai.request(server)
            .get('/employees')
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('array');
                done();
            });
    });
});